let vm = new Vue({
  el: '#app',
  data: {
    clientData:{
      firstName: 'Shirley',
      lastName: 'Thomas',
      phoneNumber: '+1 (234) 123 123',
      followUpCount: 1,
      notesCount: 3,
      profileInfo: 'Marketing Executive, Inform Group Pty Ltd, Philippines, Cosmetics',
      socialMedia: {
        Linkedin: 'http://www.linked.com', 
        Facebook: 'http://www.facebook.com',
        Twitter : 'http://www.twitter.com'
      },
      clientStatus: 'active',
      lastCall: {
        type:'Outbound',
        age: 4
      }
    },
    currentCall:{
      type: 'outbound',
      status: 'IN CALL',
      elapsedTime: '00:03:57',
      callNotes: 'Budget is finally available!  Needs to pull in some other decision makers.  Followup next week. CC sales manager @robert so he can join.'
    },
    appStrings:{
      saveButton: 'SAVE',
      showMore: 'SHOW MORE REGISTERS FROM',
      returnLink: 'Calls History',
      callLabel: 'call',
      age: 'Days Ago',
      history: 'Calls History',
      follow: 'Follow up',
      notes: 'Notes',
      addSM: '+ Add Field'
    }
  },
  methods: {
    toggleClientWindow: function(){
      var element = document.getElementById("clientWindow");
      element.classList.toggle("hidden");
    },
    closeClientWindow: function(){
      // Do Any housekeeping before closing the window: save data, clear client info, etc 
      this.toggleClientWindow();
    },
    goToCallsHistory: function(){
      // Return to call list
      console.log('Returning to call list');
    },
    saveNotes: function(){
      // Save call notes, maybe close the window too.
      console.log('Saving Notes')
    },
    goToNotes: function(){
      // Go to more call notes
      console.log('Loading Call Notes')
    },
    goToFollowUps: function(){
      // Go to more follow ups
      console.log('Loading Follow Ups')
    },
    showMoreHistory: function(){
      // Show More History
      console.log('Show More History')
    },
    addSocialMedia: function(){
      // Show a dialog to add a social media account
      console.log('Add Social Media')
    }
  }
})